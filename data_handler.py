import objects2 as obj
import dijkstras

router_list = []
link_list = []
packet_list = []

graph_list = []

pathing_dict = {}


def create_routers():
    # Router,<RouterName>,Speed,MemorySize
    file = open("routers.txt")
    data_list = file.read().split("\n")

    print("-.-.-.-.-.-.-.-.-Initialising Routers-.-.-.-.-.-.-.-.-\n")  
    for x in data_list:
        split_list = x.split(",")
        new_router = obj.Router(split_list[1], split_list[2], split_list[3])
        router_list.append(new_router)
        print(f"Router: {split_list[1]}")
    print("\n-.-.-.-.-.-.-.-.-.Routers Initialised-.-.-.-.-.-.-.-.-.")
    print(f"| Router Count: {len(router_list)}")
    print("________________________________________________________\n")


def create_links():
    # Link,<src>,Bandwidth,<dest>
    file = open("links.txt")
    data_list = file.read().split("\n")
    print(len(data_list))
    print("=.=.=.=.=.=.=.=.=.Initialising Links=.=.=.=.=.=.=.=.=.\n")  
    for x in data_list:
        split_list = x.split(",")
        new_link = obj.Link(get_node_by_name(split_list[1]), get_node_by_name(split_list[3]), split_list[2])
        link_list.append(new_link)
        print(f"Link: ({split_list[1]}) to -> ({split_list[3]}) @ {split_list[2]} Mbps")
    print("\n=.=.=.=.=.=.=.=.=.Links Initialised=.=.=.=.=.=.=.=.=.")
    print(f"| Link Count: {len(link_list)}")
    print("________________________________________________________\n")


def create_packets():
    # timestep, number of packets, size, source, dest
    file = open("packets.txt")
    data_list = file.read().split("\n")
    print("_._._._._._._._._.Initialising Wrappers_._._._._._._._._.\n")  
    for x in data_list:
        split_list = x.split(",")
        new_packet = obj.PacketWrapper(split_list[0], split_list[1], split_list[2], get_node_by_name(split_list[3]), get_node_by_name(split_list[4]))
        packet_list.append(new_packet)
        print(f"Packet: ({split_list[3]}) to -> ({split_list[4]}), Size: {split_list[2]}, Amt: {split_list[1]}")
    print("\n_._._._._._._._._.Wrappers Initialised_._._._._._._._._.")
    print(f"| Packet Count: {len(packet_list)}")
    print("________________________________________________________\n")


def calculate_paths():
    print("_._._._._._._._._.Initialising Pathing_._._._._._._._._.\n")  
    for link in link_list:
        toadd = [link.get_src().get_name(), 
                link.get_dest().get_name(), 
                link.get_speed()
                ]
        graph_list.append(toadd)

    graph = dijkstras.Graph(graph_list)

    for pw in packet_list:
        src_dest = pw.get_src().get_name() + pw.get_dest().get_name()
        if not src_dest in pathing_dict.keys():
            pathing_dict[src_dest] = graph.dijkstra(pw.get_src().get_name(), pw.get_dest().get_name())
    
    print(pathing_dict)
    print("\n_._._._._._._._._.Pathing Initialised_._._._._._._._._.")

def get_node_by_name(name):
    for router in router_list:
        if router.get_name() == name:
            return router
            
def get_link_between_nodes(node1, node2):
    for link in link_list:
        if link.is_connected(node1, node2):
            return link


def init_data():
    create_routers()
    create_links()
    create_packets()
    calculate_paths()
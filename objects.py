from queues import queue
import data_handler2
import dijkstras

packet_dict = {}

class Router:
    def __init__(self, name, speed, max_space):
        self.name = name
        self.speed = int(speed)
        self.max_space = int(max_space)
        self.used_space = 0
        self.packet_queue = queue([])
    
    def get_name(self):
        return self.name

    def get_speed(self):
        return self.speed

    def get_max_space(self):
        return self.max_space

    def get_used_space(self):
        return self.used_space

    def get_packet_queue(self):
        return self.packet_queue

    def has_packets(self):
        return self.packet_queue != []

    def how_much_can_take(self):
        if self.max_space == self.used_space:
            return 0
        return self.max_space - self.used_space

    def add_packet(self, packet, timestep):
        if self.used_space + packet.get_size() > self.max_space:
            print(f"Router {self.name} had to drop packet @ Timestep({packet.get_timestep()})")
            del packet
            return
        self.packet_queue.enqueue(packet)
        self.used_space += packet.get_size()

    def get_next_packet(self):
        return self.packet_queue.get_next()
        
    def remove_packet(self, packet):
        self.used_space -= packet.get_size()
        self.packet_queue.remove_value(packet)


class Link:
    def __init__(self, src, dest, speed):
        self.link_name = src.get_name() + speed + dest.get_name()
        self.src = src
        self.dest = dest
        self.speed = int(speed)

    def get_src(self):
        return self.src

    def get_dest(self):
        return self.dest

    def get_speed(self):
        return self.speed

    def is_connected(self, src, dest):
        return self.src == src and self.dest == dest


class PacketWrapper:
    def __init__(self, timestep, amount, size, src, dest):
        self.timestep = int(timestep)
        self.amount = int(amount)
        self.size = int(size)
        self.src = src
        self.dest = dest

    def get_timestep(self):
        return self.timestep

    def get_amount(self):
        return self.amount

    def get_size(self):
        return self.size

    def get_src(self):
        return self.src

    def get_dest(self):
        return self.dest


class Packet(PacketWrapper):
    def __init__(self, timestep, size, src, dest, unique_number, current_loc):
        super().__init__(timestep, 1, size, src, dest)
        self.packet_id = f"{src.get_name()},{dest.get_name()},{timestep}"
        self.unique_number = int(unique_number)
        self.unique_value = f"{self.packet_id},{unique_number}"
        self.current_loc = current_loc
        self.pathing_queue = data_handler2.pathing_dict[src.get_name() + dest.get_name()].copy()

    def get_next_dest(self):
        nil_node = data_handler2.get_node_by_name(self.pathing_queue[1])
        threshold = nil_node.get_max_space() * .9
        if (nil_node.get_used_space() + self.size) >= threshold:
            # A , C, D 
            # only C and D is being set as the route, removing wrongly, have to edit for all 

            graph_list = []

            for link in data_handler2.link_list:
                toadd = [link.get_src().get_name(), 
                link.get_dest().get_name(), 
                link.get_speed()
                ]
                graph_list.append(toadd)

            link = data_handler2.get_link_between_nodes(
                data_handler2.get_node_by_name(self.pathing_queue[1]),
                data_handler2.get_node_by_name(self.pathing_queue[2])
            )

            to_remove = [
                link.get_src().get_name(), 
                link.get_dest().get_name(), 
                link.get_speed()
                ]
            print(f"{to_remove} TO REMOVE")
            graph_list.remove(to_remove)

            graph = dijkstras.Graph(graph_list)

            dij = graph.dijkstra(self.src.get_name(), self.dest.get_name())

            if len(dij) == 0:
                print(len(dij))
                return data_handler2.get_node_by_name(self.pathing_queue[1])
            
            print(type(dij))

            print(f"{dij} DIJKSTRAS")

            self.pathing_queue = dij

            # recalculate
            print("ignore me rn")
        return data_handler2.get_node_by_name(self.pathing_queue[1])

    def get_id(self):
        return self.packet_id

    def get_unique_value(self):
        return self.unique_value

    def get_current_loc(self):
        return self.current_loc
        
    # Splits the packet by cloning it's values and then subtracting it's size from the main packet
    def split_packet(self, amount):
        new_packet = Packet(self.timestep, amount, self.src, self.dest, self.unique_number, self.current_loc)
        self.current_loc.get_packet_queue().insert_split(self, new_packet)
        self.size -= amount
        print(f"NEW PACKET PATH: {new_packet.pathing_queue}")
        print(f"OLD PACKET PATH: {self.pathing_queue}")
        return new_packet

    def get_destination(self):
        return self.dest

    # Adds the packet's size to it's total completion tally
    def add_completion(self):
        packet_dict[self.unique_value]['Completed'] += self.size

    # Checks if the packet's Total size equals it's Completed additions
    def check_complete(self):
        return packet_dict[self.unique_value]['Completed'] == packet_dict[self.unique_value]['Total']

    def return_split(self, togo):
        bandwidth_amount = data_handler2.get_link_between_nodes(self.current_loc, togo).get_speed()
        if not self.current_loc.get_speed() >= self.size:
            if self.current_loc.get_speed() <= bandwidth_amount:
                return self.split_packet(self.current_loc.get_speed())
            else:
                return self.split_packet(bandwidth_amount)
        else:
            if self.size <= bandwidth_amount:
                return self
        return self.split_packet(bandwidth_amount)  

    def move(self, togo, timestep):
        possible_amount = togo.how_much_can_take()

        packet_to_send = self.return_split(togo)  

        if possible_amount >= 1:

            if not possible_amount >= self.size:

                packet_to_send = self.split_packet(possible_amount) 

            if packet_to_send.get_destination() == togo:

                packet_to_send.current_loc.remove_packet(packet_to_send)

                print("packet sent to dest" + togo.get_name() + " @ " + str(timestep))

                packet_to_send.add_completion()

                print(packet_dict[packet_to_send.get_unique_value()])

                print(packet_to_send.check_complete())
                # del self
                return

            packet_to_send.pathing_queue.popleft()

            print(f"{packet_to_send.unique_value} sent from {packet_to_send.current_loc.get_name()} to {togo.get_name()}")
            
            packet_to_send.current_loc.remove_packet(packet_to_send)

            packet_to_send.current_loc = togo

            print(f"NEW PACKET PATH: {packet_to_send.pathing_queue} POS {packet_to_send.current_loc.get_name()} SIZE {packet_to_send.size}")
            print(f"a PACKET PATH: {self.pathing_queue} POS {self.current_loc.get_name()} SIZE {self.size}")

            togo.add_packet(packet_to_send, timestep)

            return
        else:
            print("cant take this fella")
    
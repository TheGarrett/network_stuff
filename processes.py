import objects2 as obj
import data_handler2

# Searches for packet to be created at given timestep

def inject_packets(timestep):
    wrapper = None
    # For every packet wrapper in the list
    for p in data_handler2.packet_list:

        # If it's timestamp is equal to the target timestep

        if p.get_timestep() == timestep:

            # Set it as our target
            wrapper = p

    # If a wrapper at the given timestamp was not found return 
      
    if wrapper is None:
        return

    target_router = wrapper.get_src()

    # Looping for i, i = how many the wrapper is creating
    for i in range(0, wrapper.get_amount()):

        # Create new packet object based off wrapper's data
        new_packet = obj.Packet(
            timestep, 
            wrapper.get_size(), 
            wrapper.get_src(), 
            wrapper.get_dest(), 
            i,
            wrapper.get_src()
        )
        print(f"injecting packet into {target_router.get_name()} - {new_packet.get_unique_value()}")
        # Add the packet to the router
        target_router.add_packet(new_packet, timestep)

        # Sets up the entry for the packet's completion tracking
        obj.packet_dict[new_packet.get_unique_value()] = {
            "Total": new_packet.get_size(),
            "Completed": 0
        }


def offload_routers(timestep):
    move_list = []
    for router in data_handler2.router_list:
        if router.has_packets():
            move_list.append(router.get_next_packet())
    for packet in move_list:
        print("\n______________________________________")
        print(f"Current packet: {packet.get_unique_value()}\n")
        print(f"Moving from: [{packet.get_current_loc().get_name()}] to [{packet.get_next_dest().get_name()}]\n")
        packet.move(packet.get_next_dest(), timestep)
        print("______________________________________\n")


def is_finished(timestep):
    for router in data_handler2.router_list:
        if router.has_packets():
            return False
    max_time = 0
    for packet_wrapper in data_handler2.packet_list:
        if packet_wrapper.get_timestep() > max_time:
            max_time = packet_wrapper.get_timestep()
    return timestep > max_time

def start_network():
    data_handler2.init_data()
    # inject_packets(1)
    # offload_routers(2)
    # offload_routers(3)
    timestep = 0
    while not is_finished(timestep):
        # print(f"===================================== \
        print(f"\n[Current Timestep ({timestep})]\n")
        offload_routers(timestep)
        inject_packets(timestep)
        timestep += 1
        # print("=====================================\n")
        print("|\nV")

if __name__ == "__main__":
    start_network()
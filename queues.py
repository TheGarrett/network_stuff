class queue(list):
    def __init__(self, input_list):
        super().__init__(input_list)

    def enqueue(self, value):
        self.append(value)

    def dequeue(self):
        try:
            return self.pop(0)
        except IndexError:
            raise IndexError("Trying to dequeue from empty queue")

    def insert_split(self, first, created):
        self[0] = first
        self.insert(1, created)

    def get_next(self):
        return self[0]

    def remove_value(self, value):
        self.remove(value)

    def isEmpty(self):
        return self == []

    def length(self):
        return len(self)

    def get_at(self, index):
        return self[index]

    def getQueue(self):
        return self